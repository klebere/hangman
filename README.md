# Hangman

## Table of Contents

* [Project Description](#project-description)
* [Run the Project](#run-the-project)
* [Use the Project](#use-the-project)
* [Game Features](#game-features)
* [Credits](#credits)

## Project Description

This program is a hangman-like game that is played in a terminal. Players guess the letters in a word. Each time a player guesses a wrong letter, they lose a “life” and a body part is added to the hangman. The game ends when the player either guesses the correct word or loses all 6 of their lives.

During the game, the program tells the players the:
* Length of the word
* What letter they guessed
* What letters they have yet to guess
* Where the letters are in the word
* How many lives they have left (with a stick-man image)
* What the word was
* When they have won or lost

At any point in the game, players can choose to guess the entire word by entering the word “guess” or choose to quit the game by entering the word “quit.”

## Run the Project

* Install Python 3
  * [Set up the Python Environment in Microsoft Windows](https://www.py4e.com/software-win.php)
  * [Set up the Python Environment on a Macintosh](https://www.py4e.com/software-mac.php)
* Download [LIBR559C_Hangman.py](https://gitlab.com/klebere/hangman/-/blob/main/LIBR559C_Hangman.py)
     * **Optional:** Download [animal.txt](https://gitlab.com/klebere/hangman/-/blob/df66a9f6a1eddedfedb9560a628f668b7669ed3e/animal.txt). This file can be used for the Single Player mode.

## Use the Project

1. Run the program in a terminal

```
python3 LIBR559C_Hangman.py
```

2. Read the printed instructions

```
Welcome to Hangman!

Rules:
You will be prompted to enter a letter while you guess the word.
If you get the letter incorrect, you will gain a body part.
You only have 6 chances to enter the correct letter - proceed with caution!
To guess the full word, enter 'guess'. If you get it wrong, you gain a body part.
To quit the game, type 'quit' as your guess.

First, choose if you would like your game to be one player or two player.
If you choose one player, you will be prompted to upload a .txt file.
A random word from the file will be chosen to be guessed.
If you choose two player, Player 1 will enter a word in which Player 2 will guess.

Enter 1 for one player or 2 for two player:
```

3. Choose the player mode by entering either a “1” or “2”
  * Single Player (“1”): The player will be prompted to import a text file (such as animal.txt). One word from the text file will be randomly chosen as the word to be guessed.
    * Note that the text file must be saved in the same folder as LIBR559C_Hangman.py in order to be accessible by the terminal.
  * Multiplayer (“2”): Player 1 will be prompted to input a word, in which Player 2 has to guess. After the word is entered by Player 1, a series of symbols are automatically printed so Player 2 cannot see the inputted word. 

4. Guess a letter from the alphabet

```
Guess a letter:
```
5. Read the response

```
Correct! That letter is in the word.
```

```
Incorrect. The letter [x] is not in the word.
Your hangman has gained a head. You have [#] lives left.
```

6. Repeat steps 4 and 5 until you win or lose
  * If you feel ready to guess the full word, enter “guess”
  * To quit the game, enter “quit”

```
Congrats! You have guessed the word!

The word was: [word]
```

```
Oh no! Your hangman has gained its right leg and is now complete.

          |
          |     O
          |    /|\
          |   / | \
          |    / \
          |   /   \
          |
          -----------

GAME OVER

The word was:  [word]
```

## Game Features

Constraints:
  * Words that contain an non-alphabet character cannot be used
  * More than 1 letter inputs are not allowed
  * File names that do not exist/are misspelled cannot be used
  * Guesses that are more than one letter long or are non-alphabet characters cannot be used

Words:
  * Can include words with duplicate letters
  * Each letter’s position in the word is shown 

## Credits
This project was created for LIBR 559C at the University of British Columbia's iSchool by Kate LeBere, Tara Rogic, and Jess Leja.

The in-terminal design template for the hangman graphic was created by [mikeprogrammer](https://stackoverflow.com/questions/43150445/terminal-messing-up-stick-figure-drawing). 



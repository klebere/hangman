# Hangman
# Kate LeBere, Jess Leja, Tara Rogic

#A function that will print the correct stickman according to how many lives are left
#This stickman function is used for both 1 and 2 players modes
def print_stickman():
    if lives==6:
        print(r"""
          |
          |
          |
          |
          |
          |
          |
          -----------""")
    elif lives==5:
        print('\nIncorrect. The letter' ,letter, 'is not in the word. \nYour hangman has gained a head. You have 5 lives left.')
        print(r"""
          |
          |     O
          |
          |
          |
          |
          |
          -----------""")
    elif lives==4:
        print('\nIncorrect. The letter' ,letter, 'is not in the word. \nYour hangman has gained a body. You have 4 lives left.')
        print(r"""
          |
          |     O
          |     |
          |     |
          |
          |
          |
          -----------""")
    elif lives==3:
        print('\nIncorrect. The letter' ,letter, 'is not in the word. \nYour hangman has gained its left arm. You have 3 lives left.')
        print(r"""
          |
          |     O
          |    /|
          |   / |
          |
          |
          |
          -----------""")
    elif lives==2:
        print('\nIncorrect. The letter' ,letter, 'is not in the word. \nYour hangman has gained its right arm. You have 2 lives left.')
        print(r"""
          |
          |     O
          |    /|\
          |   / | \
          |
          |
          |
          -----------""")
    elif lives==1:
        print('\nIncorrect. The letter' ,letter, 'is not in the word. \nYour hangman has gained its left leg. You have 1 lives left.')
        print(r"""
          |
          |     O
          |    /|\
          |   / | \
          |    /
          |   /
          |
          -----------""")
    elif lives==0:
        print('\nOh no! Your hangman has gained its right leg and is now complete.')
        print(r"""
          |
          |     O
          |    /|\
          |   / | \
          |    / \
          |   /   \
          |
          -----------""")
        print('\nGAME OVER')

#General instructions for all players
print('\nWelcome to Hangman!')
print("\nRules:\nYou will be prompted to enter a letter while you guess the word.\nIf you get the letter incorrect, you will gain a body part.\nYou only have 6 chances to enter the correct letter - proceed with caution!\nTo guess the full word, enter 'guess'. If you get it wrong, you gain a body part.\nTo quit the game, type 'quit' as your guess.\n")
print('\nFirst, choose if you would like your game to be one player or two player.\nIf you choose one player, you will be prompted to upload a .txt file.\nA random word from the file will be chosen to be guessed.\nIf you choose two player, Player 1 will enter a word in which Player 2 will guess.')

import string
import random

#Master list of letters, only allowed characters to be used in words
alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

#Determining which version (1 or 2 player) will be played
while True:
    try:
        player = int(input('\nEnter 1 for one player or 2 for two player: '))
        if player == 1:
            print('\nYou have chosen 1 player.')
            break
        if player == 2:
            print('\nYou have chosen 2 player.')
            break
        else:
            print('\nPlease enter either 1 or 2.')
            quit()
    except:
        print('\nPlease enter either 1 or 2.')
        quit()
#Single player: sets up the game by selecting a random word from a text file uploaded by the player
if player == 1:
    fname = input('Enter the file name: ')
    try:
        word = fname
        fhand = open(fname)
    except: #This will catch improper file names/files that are not in the correct directory
        print('File cannot be opened:', fname)
        quit()
    wordbank = list()
    for line in fhand:
        line = line.rstrip() #Removes any whitespace following the word, so only letters are included in the word
        line = line.translate(line.maketrans('', '', string.punctuation)) #Removes all punctuation or special characters
        line = line.lower() #Converts everything to lower case, not case sensitive
        wordchoice = line.split()
        for word in wordchoice:
            if not(word in wordbank):
                wordbank.append(word)
    word = random.choice(wordbank) #This will select a random word from the file selected
#2 player version - player one enters a word to initiate the game
if player == 2:
    try:
        word = input('PLAYER 1 - Enter a word: ')
        if word.isalpha():
            print('Print', word)
            print('buffering \n * \n' *10) #This will hide the entered letter from Player 2
            print('PLAYER 2 - Your turn!')
        else:
            print('There is an unknown character in your input!') #If there is anything but a letter from the alphabet, this will catch it and prompt the user to make a correction
            quit()
    except:
        print('\nPlease enter an English word.')
        quit()

#Initating the number of lives
lives = int(6)
#Creating a list of blanks that will be filled as the player guesses letters, and a list of all the letters in the word which will be used to check guesses
wordlength=len(word)
letterfill= '_' * wordlength
filllist=[] #This will be filled in as the player guesses
print_stickman()
for space in letterfill:
    filllist.append(space)
print('\n\nThe word is', wordlength, 'letters long:',filllist) #This will tell the player the word length
wordlist=[] #This shows the letters that make up the word using underscores
for letter in word:
    wordlist.append(letter)
#This loop that takes player guesses in both 1 and 2 player versions
while True:
    letter = str(input('\nGuess a letter: '))
    letter = letter.lower() #Controls for case
    letterinput = len(letter)
    if letter == 'guess': #If the player wants to guess the full word, they can make this entry instead of a letter
        guess=str(input('\nGuess the full word: '))
        if guess==word:
            print('\nCongrats! You have guessed the word!')
            break
        else:
            lives=lives-1 #If wrong, player loses a life
            print_stickman() #This calls the function defined at the beginning
            continue
    elif letter=='quit': #If you want to quit the game at any time, this can be entered at any point (except after 'guess')
        quit()
        #Ensuring that users only enter one English letter that has not yet been guessed
    elif letterinput==1 and letter in alpha:
        if letter in wordlist:
            lettercount=wordlist.count(letter)
            print('\nCorrect! That letter is in the word.')
            #Finds the index of the letter and replaces the blank in the fill list with the correct letter
            for char in wordlist:
                if char==letter:
                    pos=wordlist.index(letter)
                    filllist[pos]=letter
                    #If the letter occurs more than once in the word, this loop will split the letter list at each instance to catch the new index and calculates the difference to place it in the correct position in the filllist
                    if lettercount > 1:
                        for char in wordlist:
                            splitlist=wordlist[pos+1:]
                            if letter in splitlist:
                                pos=splitlist.index(letter)
                                diff=len(filllist)-len(splitlist)
                                pos=pos+diff
                                filllist[pos]=letter
                            else:
                                continue
        if letter not in wordlist: #This calculates a lost life if the letter is not in the word
            lives=lives-1
            print_stickman()
            if lives==0:
                print('\nThe word was: ', word) #When the player runs out of lives, the full hangman displays with this message
                quit()
        alpha.remove(letter) #This takes the incorrect letter out of the master list of letters, showing that it is not available for future guesses
        print('\nWord: ',filllist)
        print('\nUnguessed letters:', alpha)
        if filllist==wordlist: #If the player has successfully guessed all the letters and won the game
            print('\nCongrats! You have guessed the word!')
            print('\nThe word was: ', word)
            quit()
        else:
            continue
    else:
        print('Please enter only one letter from the English alphabet that you have not yet guessed.') #For invalid inputs
        continue